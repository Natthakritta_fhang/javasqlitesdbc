
import dao.UserDao;
import helper.DatabaseHelper;
import model.User;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Natthakritta
 */
public class TestDao {

    public static void main(String[] args) {
        //SelectAll
        UserDao userDao = new UserDao();
        for (User u : userDao.getAll()) {
            System.out.println(u);

        }

        //SelectOne
        User user1 = userDao.get(2);
        System.out.println(user1);

        //Insert
//        User user4 = new User("user4", "password", 2, "F");
//        User insertedUser = userDao.save(user4);
//        System.out.println(insertedUser);
        //Update
        user1.setGender("F");
        userDao.update(user1);
        User updateUser = userDao.get(user1.getId());
        System.out.println(updateUser);

        //Delete
//        User user4 = userDao.get(4);
//        userDao.delete(user4);
//        for (User u : userDao.getAll()) {
//            System.out.println(u);
//        }
        //GetOrderBy
        for (User u : userDao.getAllOrderBy("UserName", "asc")) {
            System.out.println(u);
        }

        //GetWhereOrderBy
        for (User u : userDao.getAllWhereOrderBy(" UserName like 'u%' ", " UserName asc, UserGender desc ")) {
            System.out.println(u);
        }

        //Close
        DatabaseHelper.close();
    }
}
